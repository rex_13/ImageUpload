# syntax=docker/dockerfile:1
FROM python:3.7-alpine

WORKDIR /app
ENV VIRTUAL_ENV=/app/venv
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
# ENV FLASK_APP=app.py
# ENV FLASK_RUN_HOST=0.0.0.0

COPY . .
RUN pip install flask
EXPOSE 5000

RUN . venv/bin/activate 

CMD [ "python", "-m", "flask", "run", "--host=0.0.0.0"]

# python -m flask --debug -A  app run --host=0.0.0.0